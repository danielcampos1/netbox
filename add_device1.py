import pynetbox
import time

#It function that connect to netbox and add devices
def adddev(dev):
    nb = pynetbox.api(url='http://192.168.140.11/',
                  token='c377aa9cf956d89ccaa119b44c93b45aa05717ca')

    result = nb.dcim.devices.create(
        name=dev,
        device_type=24,
        device_role=21,
        site=9,
    )
    print(result)
#Populate devices in file hosts.txt. 
#Example:
#Device1::192.168.0.1
#Device2::192.168.0.2
file1 = open('c:\\temp\\git\\hosts.txt', 'r')
Lines = file1.readlines()
count = 0
# Strips the newline character
for line in Lines:
    count += 1
    time.sleep(0.5)
    s = line
    l = s.split('::')
    print(l[0])
    dev = l[0]
    adddev(dev)
